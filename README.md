# Prototyp Wheelmap

Wheelmap ist eine Karte zum Suchen und Finden rollstuhlgerechter Orte. Wie bei Wikipedia können alle mitmachen und öffentlich zugängliche Orte entsprechend ihrer Rollstuhlgerechtigkeit bewerten – weltweit. Markiert wird nach dem einfachen Ampelsystem: Grün = voll rollstuhlgerecht Gelb = Teilweise rollstuhlgerecht Rot = nicht rollstuhlgerecht Unmarkierte Orte sind grau gekennzeichnet und können von jeder Person schnell und einfach markiert werden. Die so gemeinsam gesammelten Informationen sind frei zugänglich, einfach zu verstehen und können jederzeit geteilt werden. Wheelmap.org basiert auf der freien Weltkarte OpenStreetMap und bindet 180 verschiedene Ortstypen ein, die durch die farblichen Symbole (“Icons”) dargestellt werden. Eine Übersicht findest du in der Liste aller verwendeten Icons und Ortstypen. Wheelmap.org gibt es als Anwendung im Internet-Browser oder als App für iPhone und Android. Sie ist in 32 Sprachen übersetzt und funktioniert weltweit.

Mittels der Application Builder Platform Budibase wurde im Corporate Design der Hansestadt Lübeck eine Website erstellt, in welcher die Wheelmap als Widget per iFrame eingebunden wurde. Die Wheelmap ist damit voll funktionsfähig, für die Bürger unter dem Label von der Hansestadt Lübeck nutzbar und per smart-hl.city Domain erreichbar.

<img src="https://gitlab.com/travekom-open-source/smart-city-hansestadt-l-beck/prototyp-wheelmap/-/raw/main/Prototyp_Wheelmap.png">

## Installation
Zur Installation der App kann Budibase in der Cloud- oder onPremise Variante genutzt werden. Beim Erstellen einer neuen App kann dann das hier hinterlegte Archiv importiert werden.

## Nutzung
Die App ist nicht für eine produktive Nutzung gedacht, sondern wurde im Rahmen einen Domain Prototypings erstellt um als Grundlage für weitere Projekte zu dienen. Das per iFrame eingebundene Widget von wheelmap.org muss bei produktiver Nutzung gegen eine jährliche Gebühr beim Anbieter freigeschaltet werden.

## Support
Anfragen zum Projekt an team_trk_udp@travekom.de

## Contributing
Veränderungen am Sourcecode können in Rahmen von Feature-Branches mit anschließendem MergeRequest vorgenommen werden. Der MergeReqeust wird von einem Maintainer der reviewed.

## License
Das Projekt steht unter der APache 2.0 Lizenz